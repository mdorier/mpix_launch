
# Useful variables can be set in the environment:
# CC CFLAGS LDFLAGS ENABLE_SHARED

$(warning CC=$(CC))

ifeq ($(ENABLE_SHARED),)
	ENABLE_SHARED = 0
endif
ifeq ($(ENABLE_SHARED),1)
	PIC = -fPIC
else
	PIC =
endif

INCLUDES = -Isrc

CFLAGS += $(PIC) $(INCLUDES)

all: src/MPIX_Comm_launch.o

examples: example/parent example/parent-envs example/child

example/%: example/%.o src/MPIX_Comm_launch.o
	$(CC) -g -o $@ $^ $(PIC) $(LDFLAGS)

clean:
	rm -rfv example/parent example/child example/*.o src/*.o
