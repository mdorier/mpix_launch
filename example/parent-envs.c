#include <stdlib.h>
#include <stdio.h>
#include "MPIX_Comm_launch.h"

int main(int argc, char** argv) {
	MPI_Init(&argc, &argv);

	char* argvc[] = { NULL };
	int e;
/*
	MPIX_Launch("./child", 2, argvc, MPI_COMM_WORLD, MPI_INFO_NULL, &e);
*/
	MPI_Info info;
	MPI_Info_create(&info);
//	MPI_Info_set(info,"launcher","turbine");
//	MPIX_Comm_launch("test.tic", argvc, info, 0, MPI_COMM_WORLD, &e);
	MPI_Info_set(info,"output","myoutputfile");
	MPI_Info_set(info,"envs","4");
	MPI_Info_set(info,"env0","var0=value0");
	MPI_Info_set(info,"env1","var11=value11");
	MPI_Info_set(info,"env2","v=z");
	MPI_Info_set(info,"env3","v=zxv");
	MPI_Info_set(info,"chdir","/tmp2");

	MPIX_Comm_launch("printenv", argvc, info, 0, MPI_COMM_WORLD, &e);
	printf("Exit code: %d\n",e);
	MPI_Info_free(&info);
	MPI_Finalize();
	return 0;
}
