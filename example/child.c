#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {

	MPI_Init(&argc, &argv);

	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	printf("%d: Hello from rank %d\n", rank, rank);
	int i;
	for(i=1; i<argc; i++) {
		printf("%d: argv[%d] = %s\n",rank,i,argv[i]);
	}

	MPI_Finalize();
	return 0;
}
