#include <stdlib.h>
#include <stdio.h>
#include "MPIX_Comm_launch.h"

int main(int argc, char** argv) {
	MPI_Init(&argc, &argv);
        int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	char* argvc[] = { "abc" , "defg", NULL };
	int e;

        /* CASE 1: Default info settings */
           MPIX_Comm_launch("./child", argvc, MPI_INFO_NULL, 0, MPI_COMM_WORLD, &e);

        /* CASE 2: Use some info settings
	MPI_Info info;
	MPI_Info_create(&info);
	MPI_Info_set(info,"output","myoutputfile");
	MPIX_Comm_launch("./child", argvc, info, 0, MPI_COMM_WORLD, &e);
        MPI_Info_free(&info);
        */

        /* CASE 3: Subdivide first
        MPI_Comm subcomm;
        MPI_Comm_split(MPI_COMM_WORLD, rank % 2, 0, &subcomm);
        MPIX_Comm_launch("./child", argvc, MPI_INFO_NULL, 0, subcomm, &e);
        MPI_Comm_free(&subcomm);
        */

        if (rank == 0)
          printf("Exit code: %d\n",e);
	MPI_Finalize();
	return 0;
}
