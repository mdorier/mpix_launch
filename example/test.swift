import io;
import sys;

app (void signal) sleep (int secs) {
  "/bin/sleep" secs
}

(int out) funA
{
   @prio=1 printf("AAA") =>
   out = 1;
}

funB
{
   @prio=2 sleep(1) =>
   printf("BBB");
}

printf("Number of workers is %d",turbine_workers());
printf("Number of ADLB servers is %d",adlb_servers());

foreach i in [0:10:1] {
   @prio=1 funA() =>
   @prio=2 funB();
}
