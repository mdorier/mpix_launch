
## Makefile

The `Makefile` optionally supports `CC`, `CFLAGS`, `LDFLAGS`, and `ENABLE_SHARED`.

For example, on a Cray, you need to do:

~~~~
$ MPI=/opt/cray/mpt/default/gni/mpich2-gnu/49
$ export CFLAGS=-I$MPI/include
$ export LDFLAGS="-L$MPI/lib -lmpich"
$ export ENABLE_SHARED=1

$ make
~~~~

Put that in a shell script to avoid cluttering your environment.

## Typical usage (MPICH)

~~~~
$ CC=mpicc make examples
$ mpiexec -n 2 example/child
0: Hello from rank 0
1: Hello from rank 1
# The child works!
$ CC=mpicc make example/parent
$ cd example
$ mpiexec -n 4 ./parent
~~~~

There are 3 cases.  Simply edit the commented section in parent.c to change them.

* Case 1: Simply invoke parent->child once.
* Case 2: Set up an info variable and pass it to the child.
* Case 3: Do a MPI_Comm_split() first, thus invoking the child on each subcommunicator.
